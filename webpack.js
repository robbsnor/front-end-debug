const path = require("path");

module.exports = {
  mode: "development",
  entry: {
    main: "./src/js/dev.js",
  },
  output: {
    path: path.join(__dirname, "./dist/"),
    filename: "dev.js",
  },
};
