// gulp
const gulp = require("gulp");
const sass = require("gulp-sass");
const clean = require("gulp-clean");
const sourcemaps = require("gulp-sourcemaps");
const autoprefixer = require("gulp-autoprefixer");
const browserSync = require("browser-sync").create("bsServer");

// webpack
const webpack = require("webpack-stream");
const webpackConfig = require("./webpack");

// vars
const justMoveFileTypes = "html,svg";

// dev tasks
function compileScss() {
  return (
    gulp
      .src("./src/**/dev.scss")
      .pipe(sourcemaps.init())
      .pipe(sass().on("error", sass.logError))
      // .pipe(autoprefixer())
      .pipe(sourcemaps.write())
      .pipe(gulp.dest("./dev/"))
      .pipe(browserSync.stream())
  );
}

function compileWebpack() {
  return webpack(webpackConfig)
    .pipe(gulp.dest("./dev/js/"))
    .pipe(browserSync.stream());
}

function moveFiles() {
  return gulp
    .src("./src/**/*.{" + justMoveFileTypes + "}")
    .pipe(gulp.dest("./dev/"))
    .pipe(browserSync.stream());
}

function moveDev() {
  return gulp
    .src("./src/**/dev.js")
    .pipe(gulp.dest("./dev/"))
    .pipe(browserSync.stream());
}

function deleteDev() {
  return gulp.src("./dev", { read: false, allowEmpty: true }).pipe(clean());
}

// browsersync
function bsServe() {
  browserSync.init({
    server: {
      baseDir: "./dev",
      index: "/index.html",
    },
    // open: "external",
    open: false,
    notify: false,
    ghostMode: false,
  });

  gulp.watch("./src/**/*.{" + justMoveFileTypes + "}", moveFiles);
  gulp.watch("./src/**/*.scss", compileScss);
  gulp.watch("./src/**/*.js", compileWebpack, moveDev);
}

exports.start = gulp.series(
  deleteDev,
  gulp.parallel(moveFiles, compileScss, moveDev, compileWebpack),
  bsServe
);
