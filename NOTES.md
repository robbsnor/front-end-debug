## Good to know
The compilers are only used for development purpouse, for production the user needs to include the *'front-end-debug.scss'* and the *'front-end-debug.js'*

## Getting started
To start detiting files run
```
npm i
```
```
npm run start
```

## Publish
Change the version number in *'package.json'*

Publish files to the public
```
npm publish
```


## Fonts
https://fonts.googleapis.com/css?family=Material+Icons+Round|Material+Icons+Outlined