export var pannelTemplate = `
	<div class="fed__pannel">
		<div class="fed__bridge">
			<div class="fed__bridge__collumns"></div>
		</div>
		<div class="fed__pannel__item">
			<i class="fed__pannel__icon fed__pannel__icon--smaller"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bold"><path d="M6 4h8a4 4 0 0 1 4 4 4 4 0 0 1-4 4H6z"></path><path d="M6 12h9a4 4 0 0 1 4 4 4 4 0 0 1-4 4H6z"></path></svg></i>
			<div class="fed__pannel__value fed__breakpoints-indicator"></div>
		</div>
		<div class="fed__pannel__item">
				<i class="fed__pannel__icon fed__pannel__icon--smaller"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M18.657 7.70728L22.8996 11.9499L18.657 16.1926" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M5.24268 16.1926L1.00004 11.9499L5.24268 7.70728" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M1.00031 11.9496L22.9 11.9496" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg></i>
			<div class="fed__pannel__value fed__width-indicator">-</div>
		</div>
		<div class="fed__pannel__item">
			<div class="fed__rotate-90">
				<i class="fed__pannel__icon fed__pannel__icon--smaller"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M18.657 7.70728L22.8996 11.9499L18.657 16.1926" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M5.24268 16.1926L1.00004 11.9499L5.24268 7.70728" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M1.00031 11.9496L22.9 11.9496" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg></i>
				</div>
			<div class="fed__pannel__value fed__height-indicator">-</div>
		</div>
		<div class="fed__pannel__item">
			<i class="fed__pannel__icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevrons-down"><polyline points="7 13 12 18 17 13"></polyline><polyline points="7 6 12 11 17 6"></polyline></svg></i>
			<div class="fed__pannel__value fed__scrolltop-indicator">-</div>
		</div>
		<div class="fed__pannel__button fed__grid--toggle">
			<div class="fed__rotate-90">
				<i class="fed__pannel__icon fed__pannel__icon--smaller"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-align-justify"><line x1="21" y1="10" x2="3" y2="10"></line><line x1="21" y1="6" x2="3" y2="6"></line><line x1="21" y1="14" x2="3" y2="14"></line><line x1="21" y1="18" x2="3" y2="18"></line></svg></i>
			</div>
		</div>
		<div class="fed__pannel__button fed__pannel__toggle-button fed__pannel--toggle">
			<i class="fed__pannel__icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg></i>
		</div>
	</div>
`;

export var gridTemplate = `
	<div class="fed__grid">
		<div class="fed__grid__container container">
			<div class="fed__grid__row row">
			</div>
		</div>
	</div>
`;

export var colTemplate = `
	<div class="col">
		<div class="fed__grid__col"></div>
	</div>
`;
