import { pannelTemplate } from "./_templates";

export class Pannel {
  constructor(shortcutKey = "f") {
    this.element = ".fed__pannel";
    this.activeClass = "fed__pannel--open";
    this.isOpen;
    this.shortcutKey = shortcutKey;
    this.template = pannelTemplate;
    this.widthIndicatorEl = ".fed__width-indicator";
    this.heightIndicatorEl = ".fed__height-indicator";
    this.scrolltopIndicatorEl = ".fed__scrolltop-indicator";

    this.init();
  }

  renderTemplate() {
    document.body.innerHTML += this.template;
  }

  open() {
    var element = document.querySelector(this.element);

    element.classList.add(this.activeClass);
    element.style.cssText = "left: 0;";

    this.isOpen = true;
  }
  close() {
    var element = document.querySelector(this.element);
    var pannelWidth = element.offsetWidth;
    var btnWidth = document.querySelector(".fed__pannel__toggle-button")
      .offsetWidth;
    var offset = pannelWidth - btnWidth;

    element.classList.remove(this.activeClass);
    element.style.cssText = "left: " + -offset + "px;";

    this.isOpen = false;
  }
  toggle() {
    var element = document.querySelector(this.element);

    if (this.isOpen == true) {
      this.close();
    } else {
      this.open();
    }
  }

  setWidthIndicator() {
    var indicator = document.querySelector(this.widthIndicatorEl);

    indicator.innerHTML = window.innerWidth;
  }
  setHeightIndicator() {
    var indicator = document.querySelector(this.heightIndicatorEl);

    indicator.innerHTML = window.innerHeight;
  }
  setScrollIndicator() {
    var indicator = document.querySelector(this.scrolltopIndicatorEl);
    var distance = Math.floor(document.documentElement.scrollTop);

    indicator.innerHTML = distance;
  }

  init() {
    this.renderTemplate();
    this.setWidthIndicator();
    this.setHeightIndicator();
    this.setScrollIndicator();
    this.open();
  }
}
