import { pannel } from './_pannel';
import { gridOverlay } from './_grid';



document.onkeypress = function (e) {
    var key = e.key

    var pannelKey = pannel.shortcutKey
    var gridKey = gridOverlay.shortcutKey
    
    if (key == pannelKey || key == pannelKey.toUpperCase) {
        pannel.toggle()
    }

    if (key == gridKey || key == gridKey.toUpperCase) {
        gridOverlay.toggle()
    }
};