// import './_keyboard-shortcuts'
import { Pannel } from "./_pannel";
import { Grid } from "./_grid";

export default class Fed {
  constructor() {
    this.pannel = new Pannel();
    this.grid = new Grid();

    window.addEventListener("resize", () => {
      this.pannel.setWidthIndicator();
      this.pannel.setHeightIndicator();
    });
    window.addEventListener("scroll", () => {
      this.pannel.setScrollIndicator();
    });
    document
      .querySelector(".fed__grid--toggle")
      .addEventListener("click", () => {
        this.grid.toggle();
      });
    document
      .querySelector(".fed__pannel--toggle")
      .addEventListener("click", () => {
        this.pannel.toggle();
      });
  }
}
