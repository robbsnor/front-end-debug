
// get collumns amount
function getColLength() {
    
    // get css content value
    var content = window.getComputedStyle(
        document.querySelector('.fed__bridge__collumns'), ':after'
    ).getPropertyValue('content')

    // remove quotes
    var colLength = content.replace(/['"]+/g, '')
    
    return colLength;
}



export { getColLength }