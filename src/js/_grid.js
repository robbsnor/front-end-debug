import { gridTemplate, colTemplate } from './_templates'
import { getColLength } from './_bridge'



export class Grid {
    constructor(
        shortcutKey = 'g'
    )
    {
        this.element = '.fed__grid'
        this.activeClass = 'fed__grid--show'
        this.shortcutKey = shortcutKey
        this.template = gridTemplate

        this.init()
    }

    renderTemplate() {
        document.body.innerHTML += this.template
    }
    generateCols() {
        var columns = getColLength()
        
        for (var i = 0; i < columns; i++) {
            document.querySelector('.fed__grid__row').innerHTML += colTemplate
        }
    }

    show() {
        var element = document.querySelector(this.element)
        element.classList.add(this.activeClass)

        this.isOpen = true
    }
    hide() {
        var element = document.querySelector(this.element)
        element.classList.remove(this.activeClass)

        this.isOpen = false
    }
    toggle() {
        var element = document.querySelector(this.element)

        if (this.isOpen == true) {
            this.hide()
        } else {
            this.show()
        }
    }
    
    init() {
        this.renderTemplate()
        this.generateCols()
    }
}