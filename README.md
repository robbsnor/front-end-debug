# !! THIS PACKAGE IS A WORK IN PROGRESS !!

# A front-end debug pannel
A pannel with nifty features

- Dynamic Bootstrap breakpoints
- Window width indicator
- Window height indicator
- Window scrollTop indicator

## Getting started

1. Install front-end-debug
```
npm i front-end-debug
```

2. Include the SCSS file, make sure to import Bootstrap first
```scss
@import 'node_modules/front-end-debug/src/scss/front-end-debug';
```

3. Import the JS file, but only on Webpack's development mode
```js
if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    require('front-end-debug');
}
```